import { IMenu } from 'app/shared/model/menu.model';
import { IOrder } from 'app/shared/model/order.model';

export interface ICooperative {
  id?: number;
  name?: string | null;
  address?: string;
  menus?: IMenu[] | null;
  orders?: IOrder[] | null;
}

export const defaultValue: Readonly<ICooperative> = {};
